'use strict';

var Handlebars = require('handlebars');
var Hapi = require('hapi');
var Path = require('path');
var Inert = require('inert');

// creating the hapi server instance
var server = new Hapi.Server();

// adding a new connection that can be listened on
server.connection({
  port: 3000,
  host: 'localhost'
});

server.register({
            register: require('vision'),
            once: true
        },
        function (err) {
            if (err) {
                server.log(['user-basic', 'vision', 'error'], err);
            } else {
                server.views({
                    engines: {
                        html: require('handlebars')
                    },
                    path: Path.join(__dirname, 'assets/views')
                });
            }
        });

var routes = [
	{
        method: 'GET',
        path: '/',
        handler: function (request, reply) {
        	var data = {
        		title: 'Item Title',
        		body: 'Item Body'
        	}
            reply.view('layout/default.html', data);
        }
    },
    {
        method: 'GET',
        path: '/{filename}',
        handler: {
            file: function (request) {
                console.log('request.params.filename: ' + '/assets/' + request.params.filename);
                return request.params.filename;
            }
        }
    },
    {
        method: 'GET',
        path: '/js/{filename*}',
        handler: {
            directory: {
                path: 'assets/js'
            }
        }
    },
    {
        method: 'GET',
        path: '/sass/{filename*}',
        handler: {
            directory: {
                path: 'assets/sass'
            }
        }
    },
    {
        method: 'GET',
        path: '/img/{filename*}',
        handler: {
            directory: {
                path: 'assets/images'
            }
        }
    }
]

server.register(require('inert'), (err) => {

    if (err) {
        throw err;
    }

    server.route(routes);

    server.start((err) => {

        if (err) {
            throw err;
        }

        console.log('Server running at:', server.info.uri);
    });
});